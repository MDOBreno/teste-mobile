//
//  DateExtensao.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 11/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation


extension Date {
    func dataFormatadaCurta() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.string(from: self)
    }
    func dataFormatada() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ", dd 'de' "
        var dataString = self.diaDaSemana()! + dateFormatter.string(from: self) + self.mes()!
        dateFormatter.dateFormat = " 'de' yyyy 'às' HH:mm"
        dataString.append(contentsOf: dateFormatter.string(from: self))
        return dataString
    }
    
    func dataCadastro() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dataString = dateFormatter.string(from: self)
        return dataString
    }
    
    func dataServer() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var dataString = dateFormatter.string(from: self) + " 00:00:00"
        return dataString
    }
    
    func diaDaSemana() -> String? {
        switch Calendar.current.dateComponents([.weekday], from: self).weekday {
        case 1:
            return "Domingo"
        case 2:
            return "Segunda-Feira"
        case 3:
            return "Terça-Feira"
        case 4:
            return "Quarta-Feira"
        case 5:
            return "Quinta-Feira"
        case 6:
            return "Sexta-Feira"
        case 7:
            return "Sabado"
        default:
            return ""
        }
    }
    func mes() -> String? {
        switch Calendar.current.dateComponents([.month], from: self).month {
        case 1:
            return "Janeiro"
        case 2:
            return "Fevereiro"
        case 3:
            return "Março"
        case 4:
            return "Abril"
        case 5:
            return "Maio"
        case 6:
            return "Junho"
        case 7:
            return "Julho"
        case 8:
            return "Agosto"
        case 9:
            return "Setembro"
        case 10:
            return "Outubro"
        case 11:
            return "Novembro"
        case 12:
            return "Dezembro"
        default:
            return ""
        }
    }
    func addAnos(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .year, value: n, to: self)!
    }
    func addMeses(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .month, value: n, to: self)!
    }
    func addDias(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .day, value: n, to: self)!
    }
    func addHoras(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .hour, value: n, to: self)!
    }
    func addMinutos(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .minute, value: n, to: self)!
    }
    func addSegundos(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .second, value: n, to: self)!
    }
}
