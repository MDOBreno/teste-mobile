//
//  UseCase.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation

protocol RequestValuesProtocol{}
protocol ResponseValueProtocol{}

protocol UseCase{
    func executeUseCase(requestValues: RequestValuesProtocol, onComplete: @escaping (ResponseValueProtocol) -> Void, onError: @escaping (Constants.ComunicationError) -> Void)
}
