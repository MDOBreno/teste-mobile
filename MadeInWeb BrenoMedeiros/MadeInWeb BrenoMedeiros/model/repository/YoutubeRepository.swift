//
//  YoutubeAPI.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 09/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation
import SwiftHash
import Alamofire

class YoutubeAPI {
    
    static private let basePath = "https://www.googleapis.com/youtube/v3/search?part=snippet&"
    static private let Key = "AIzaSyDx4x-heLN8tMDM4uzEtHM_hVe4h9Ixr_4"
    static private let resultsPerPage = 50
    
    class func loadYoutubes(name: String?, page: Int=1, onComplete: @escaping (YoutubeJson?) -> Void) {
        
        let maxResults = page * resultsPerPage
        let ordemTipo = "order=relevance&type=video&"
        let startsWith: String
        if let name=name, !name.isEmpty {
            startsWith = "q=\(name.replacingOccurrences(of: " ", with: "+"))&key="
        } else {
            startsWith = ""
        }
        var url: String
        
        // TENTATIVA com api oficial
        var sucessoNaTentativa = true
        url = basePath + "maxResults=\(maxResults)&" + ordemTipo + startsWith + Key
        print("Tentando: " + url)
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
                
            }
            do {
                let youtubeJson = try JSONDecoder().decode(YoutubeJson.self, from: data)
                onComplete(youtubeJson)
            } catch {
                print(error.localizedDescription)
                onComplete(nil)
                sucessoNaTentativa = false
            }
        }
        //TENTATIVA com consulta fixa para "Ricardo Milos"
        if (!sucessoNaTentativa) {
            url = "https://sendeyo.com/up/d/4d42b32ff9"
            print("Consulta alternativa utilizada: " + url)
            Alamofire.request(url).responseJSON { (response) in
                guard let data = response.data else {
                    onComplete(nil)
                    return
                    
                }
                do {
                    let youtubeJson = try JSONDecoder().decode(YoutubeJson.self, from: data)
                    onComplete(youtubeJson)
                } catch {
                    print(error.localizedDescription)
                    onComplete(nil)
                }
            }
        }
        
    }
}
