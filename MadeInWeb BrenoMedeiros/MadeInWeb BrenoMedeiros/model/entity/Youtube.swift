//
//  YoutubeClasses.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 09/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation

struct YoutubeJson: Codable {
    let pageInfo: YoutubeInfo
    let items: [YoutubeData]
}

struct YoutubeInfo: Codable {
    let totalResults: Int
    let resultsPerPage: Int
}

struct YoutubeData: Codable {
    let snippet: Youtube
}

struct Youtube: Codable {
    let publishedAt: String
    let channelId: String
    let title: String
    let description: String
    let thumbnails: Thumbnail
}

struct Thumbnail: Codable {
    let medium: Imagem
}

struct Imagem: Codable {
    let url: String
}
