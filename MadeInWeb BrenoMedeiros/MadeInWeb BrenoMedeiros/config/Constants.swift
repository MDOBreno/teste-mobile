//
//  Constants.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation

class Constants{
    
    enum ComunicationError {
        
        case url
        
        case taskError(error : Error)
        
        case noResponse
        
        case noData
        
        case responseStatusCode(code: Int)
        
        case invalidJSON
        
    }
    
}
