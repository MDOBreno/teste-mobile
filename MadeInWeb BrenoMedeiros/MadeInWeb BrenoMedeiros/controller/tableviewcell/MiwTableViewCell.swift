//
//  HeroTableViewCell.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 09/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import UIKit
import Kingfisher

class MiwTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivThumb: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func prepareCell(with youtube: Youtube) {
        lbName.text = youtube.title
        lbDescription.text = youtube.description
        if let url=URL(string:youtube.thumbnails.medium.url) {
            ivThumb.kf.indicatorType = .activity
            ivThumb.kf.setImage(with: url)
        } else {
            ivThumb.image = nil
        }
        ivThumb.layer.cornerRadius = ivThumb.frame.size.height/10
        ivThumb.layer.borderColor = UIColor.blue.cgColor
        ivThumb.layer.borderWidth = 1
        
    }
    
}
