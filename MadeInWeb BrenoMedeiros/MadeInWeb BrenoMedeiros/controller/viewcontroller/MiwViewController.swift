//
//  MiwViewController.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import UIKit
import WebKit

class MiwViewController: UIViewController {

    @IBOutlet weak var ivVideo: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbData: UILabel!
    @IBOutlet weak var lbDescricao: UILabel!
    
    var data: Date!
    var youtube: Youtube!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        
        lbTitulo.text = youtube.title
        
        lbDescricao.text = youtube.description
        
        if let url=URL(string:youtube.thumbnails.medium.url) {
            ivVideo.kf.indicatorType = .activity
            ivVideo.kf.setImage(with: url)
        } else {
            ivVideo.image = nil
        }
        ivVideo.layer.cornerRadius = ivVideo.frame.size.height/20
        ivVideo.layer.borderColor = UIColor.black.cgColor
        ivVideo.layer.borderWidth = 1
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var dataAux = youtube.publishedAt.prefix(19).replacingOccurrences(of: "T", with: " ")
        data = dateFormatter.date(from: dataAux)!
        lbData.text = data.dataFormatada()
        
        lbDescricao.text = youtube.description
        
        
        let url = URL(string: youtube.thumbnails.medium.url)
        let request = URLRequest(url: url!)
        title = ""
        
        let cabecalho = UIImageView(image: UIImage(named: "madeinweb.png"))
        cabecalho.isUserInteractionEnabled = true
        let unicoToque: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.toqueCabecalho))
        unicoToque.numberOfTapsRequired = 1;
        cabecalho.addGestureRecognizer(unicoToque)
        cabecalho.contentMode = .scaleAspectFit
        self.navigationItem.titleView = cabecalho
    }
    @objc func toqueCabecalho(sender:UITapGestureRecognizer) {
        _ = navigationController?.popViewController(animated: true)
    }

    
    
}


