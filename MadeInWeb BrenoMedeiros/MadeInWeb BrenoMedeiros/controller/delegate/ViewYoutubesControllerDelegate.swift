//
//  ViewYoutubesControllerDelegate.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation

protocol ViewYoutubesControllerDelegate: NSObjectProtocol{
    func showLoading()
    func removeLoading()
    func showYoutubes(novosVideos: [Youtube])
    func showString(stringToShow: String)
}

protocol ViewYoutubesPresenterDelegate{
    func loadData()
}
