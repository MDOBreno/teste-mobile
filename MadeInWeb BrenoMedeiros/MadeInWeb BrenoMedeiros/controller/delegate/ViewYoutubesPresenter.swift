//
//  ViewYoutubesPresenter.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation

class ViewYoutubesPresenter: ViewYoutubesPresenterDelegate{
    
    var mGetYoutubes: GetYoutubes
    weak private var mViewYoutubesDelegate: ViewYoutubesControllerDelegate?
    
    init(getYoutubes: GetYoutubes){
        self.mGetYoutubes = getYoutubes
    }
    
    func setViewDelegate(viewYoutubesDelegate: ViewYoutubesControllerDelegate?){
        self.mViewYoutubesDelegate = viewYoutubesDelegate
    }
    
    func loadData() {
        let requestValues = GetYoutubes.RequestValues()
        mGetYoutubes.executeUseCase(requestValues: requestValues, onComplete: {(Response) in
            DispatchQueue.main.async {
                self.mViewYoutubesDelegate?.removeLoading()
                let response = Response as! GetYoutubes.ResponseValue
                if(response.mList.count > 0){
                    self.mViewYoutubesDelegate?.showYoutubes(novosVideos: response.mList)
                }else{
                   self.mViewYoutubesDelegate?.showString(stringToShow: "Nenhum Video")
                }
            }
        }, onError: {(Error) in
            DispatchQueue.main.async {
                self.mViewYoutubesDelegate?.removeLoading()
                self.mViewYoutubesDelegate?.showString(stringToShow: "Error")
            }
        })
    }
}
