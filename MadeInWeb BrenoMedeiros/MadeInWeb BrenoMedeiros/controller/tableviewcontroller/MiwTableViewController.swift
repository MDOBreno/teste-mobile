//
//  MiwTableViewController.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import UIKit

class MiwTableViewController: UITableViewController, ViewYoutubesControllerDelegate {

    var name: String?
    var youtubes: [Youtube] = []
    var filtredEntityArray: [Youtube] = []
    var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .blue
        return label
    }()
    var indicator = UIActivityIndicatorView()
    var loadingYoutubes = false
    var currentPage = 1
    var totalResults = 0
    var searchController =  UISearchController(searchResultsController: nil)
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true 
        
        label.text = "Buscando videos, Aguarde..."
        loadYoutubes()
        title = ""
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Buscar Video"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        let cabecalho = UIImageView(image: UIImage(named: "madeinweb.png"))
        cabecalho.isUserInteractionEnabled = true
        let unicoToque: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.toqueCabecalho))
        unicoToque.numberOfTapsRequired = 1;
        cabecalho.addGestureRecognizer(unicoToque)
        cabecalho.contentMode = .scaleAspectFit
        self.navigationItem.titleView = cabecalho
    }
    @objc func toqueCabecalho(sender:UITapGestureRecognizer) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MiwViewController
        let youtubeEscolhido: Youtube
        if isFiltering {
            youtubeEscolhido = filtredEntityArray[tableView.indexPathForSelectedRow!.row]
        } else {
            youtubeEscolhido = youtubes[tableView.indexPathForSelectedRow!.row]
        }
        vc.youtube = youtubeEscolhido
        
        let backItem = UIBarButtonItem()
        backItem.title = "Voltar"
        navigationItem.backBarButtonItem = backItem
    }
    
    func loadYoutubes() {
        loadingYoutubes = true
        YoutubeAPI.loadYoutubes(name: name, page: currentPage) { (info) in
            if let info = info {
                var youtubesNovos: [Youtube] = []
                for ytNovo in info.items {
                    var youtubeAux: [Youtube] = [ytNovo.snippet]
                    youtubesNovos += youtubeAux
                }
                self.youtubes += youtubesNovos
                self.totalResults = info.pageInfo.totalResults
                print("Total: ", self.totalResults, "- Já incluídos: ", self.youtubes.count)
                DispatchQueue.main.async {
                    self.loadingYoutubes = false
                    self.label.text = "Não foram encontrados videos com o nome \(self.name!)."
                    self.tableView.reloadData()
                }
            }
        }
        
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = youtubes.count==0 ? label : nil
        
        if isFiltering {
          return filtredEntityArray.count
        }
        return youtubes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiwTableViewCell
        
        let youtube: Youtube
        if isFiltering {
          youtube = filtredEntityArray[indexPath.row]
        } else {
          youtube = youtubes[indexPath.row]
        }
        
        cell.prepareCell(with: youtube)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == youtubes.count-10 && !loadingYoutubes && youtubes.count != totalResults {
            currentPage += 1
            loadYoutubes()
            print("Carregando mais videos")
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    func filterContentForSearchText(filtering : String = ""){
        filtredEntityArray = []
        if(filtering.isEmpty){
            filtredEntityArray = youtubes
        }else{
            for Entity in self.youtubes{
                if(Entity.title.lowercased().contains(filtering.lowercased()) || Entity.description.lowercased().contains(filtering.lowercased())){
                    filtredEntityArray.append(Entity)
                }
            }
        }
        self.filtredEntityArray = self.filtredEntityArray.sorted(by: { $0.publishedAt > $1.publishedAt })
        self.tableView.reloadData()
    }
    
}
extension MiwTableViewController : UISearchResultsUpdating{
    
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(filtering: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filterContentForSearchText()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        filterContentForSearchText(filtering: searchBar.text!)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(filtering: searchText)
    }
    
    func showLoading() {
        indicator.startAnimating()
        indicator.color = UIColor(named: "main")
    }
    
    func removeLoading() {
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
    }
    
    func showYoutubes(novosVideos: [Youtube]) {
        self.youtubes = novosVideos
        self.filtredEntityArray = self.youtubes.sorted(by: { $0.publishedAt > $1.publishedAt })
        self.tableView.reloadData()
    }
    
    func showString(stringToShow: String) {
        //label.text = stringToShow
        //label.textColor = UIColor(named: "separador_linha")
        //label.textAlignment = .center
        //tableView.backgroundView = label
    }
}

