//
//  GetYoutubes.swift
//  MadeInWeb BrenoMedeiros
//
//  Created by Breno Medeiros on 10/01/20.
//  Copyright © 2020 ProgramasBMO. All rights reserved.
//

import Foundation
/*
 Fetches the list of noticias.
 */
class GetYoutubes: UseCase{
    
    private var mYoutubesRepository: YoutubeAPI
    
    init(youtubeRepository: YoutubeAPI){
        mYoutubesRepository = youtubeRepository
    }
    
    func executeUseCase(requestValues: RequestValuesProtocol, onComplete: @escaping (ResponseValueProtocol) -> Void, onError: @escaping (Constants.ComunicationError) -> Void) {
        YoutubeAPI.loadYoutubes(name: "", onComplete: {(Youtubes) in
            var youtubesNovos: [Youtube] = []
            for ytNovo in Youtubes!.items {
                var youtubeAux: [Youtube] = [ytNovo.snippet]
                youtubesNovos += youtubeAux
            }
            let response: ResponseValue = ResponseValue(mData: youtubesNovos)
            onComplete(response)
        })
    }
    
    class RequestValues: RequestValuesProtocol{
        init(){}
    }
    
    class ResponseValue: ResponseValueProtocol{
        var mList: [Youtube]
        init(mData: [Youtube]){
            mList = mData
        }
    }
}
