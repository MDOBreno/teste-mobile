# Teste Madeinweb: Mobile
Desenvolver uma aplicação Mobile


## Atividades Cumpridas [Instruções]
✅ Fazer um fork do projeto para a sua conta pessoal no BitBucket.
✅ Siguir as especificações.
✅ Crie um README com as instruções para compilar, testar e rodar o projeto.
✅ O link do repositório deverá ser enviado para o e-mail rh@madeinweb.com.br com o título **Teste Mobile [plataforma]** (substituir plataforma por Android/iOS/Hibrido)


## Atividades Cumpridas [Especificações tecnicas]
✅ Utilizar a [API de busca do YouTube](https://developers.google.com/youtube/v3/docs/search/list)
✅ Cores livres, imagens livres
✅ Gitflow
✅ Layout mobile: [clique aqui](https://invis.io/KUOGOG2SRXH#/324366013_01_PESQUISAR)
✅ Utilizar as specs do projeto  [aqui](https://drive.google.com/open?id=0B6TTk9-0XqYXVWFER19zZHotbkE).


## Atividades Cumpridas [Especificações funcionais]
✅ Tela Inicial
✅ Essa tela terá um formulário de busca(com validação) posicionado no meio da tela com campo de texto "Pesquisar" e um botão "Buscar".
✅ Ao fazer a busca, o formulário deve ser movido para o topo da tela usando animações e mostrar a lista de resultados com os campos título, descrição, thumbnail e um link para a página de detalhes.
✅ Chamar a url https://www.googleapis.com/youtube/v3/search?part=id,snippet&q={termo_de_busca}&key={API_KEY}
### OBS: A versão gratuita dessa APi esta deixando realizar pouquissimas requisições por dia(em torno de 5 vezes no meu experimento), então eu peguei um dos ".json" que vieram da APi  para a pesquisa "Ricardo Milos", que foi um meme em 2019 e rendeu varios videos na plataforma. Fiz o desenvolvimento para sempre que atingir for atingio o limite de consultas, ele colherá esse .json de outra url() que preparei em uma nuvem. Para ambas as consultas, suas URLs são:
#### "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=relevance&type=video&q=Ricardo+Milos&AIzaSyD_tmni9vPZfR-M7JVq41cEkN4h1A4qK1s"
#### "https://sendeyo.com/up/d/4d42b32ff9"     -->    Fixo para pesquisa "Ricardo Milos", e seus .json de resposta se encontrará na raiz do Repositorio


## Atividades Cumpridas [Tela de detalhes]
⬜️ A partir do videoId retornado na outra chamada, deve ser feito uma chamada para https://www.googleapis.com/youtube/v3/videos?id={VIDEO_ID}&part=snippet,statistics&key={API_KEY}
✅ Essa tela deve ter um botão para voltar para resultados da busca.
✅ A partir desse retorno, deve-se montar uma tela contendo embed do video, título, descrição e visualizações.
### OBS: Como não foi passado o prazo para execução dessa atividade, eu supus que seria algo em torno de 5 dias para completar a tarefa, e como viajo amanhã(sabado-11/01) e so volto na terça-feira, acabei ficando sem tempo para fazer essa segunda requisição da APi para pegar o numero de visualizações.  Porém eu substitui ele pela data de Postagem, além de preencher com a imagem do video, título e descrição para ficar igual ao video passado. 

## Atividades Cumpridas [Avaliações]
✅ Organização do projeto
✅ Lógica do código
✅ Uso do Git
✅ Qualidade visual (layout, animações nativas utilizadas)

## Atividades Cumpridas [Pontos Extras]
✅ Fazer paginação na tela de vídeos, utilizando os [recursos de paginação da api](https://developers.google.com/youtube/v3/guides/implementation/pagination?hl=pt-br).
⬜️ Utilizar animações para os novos resultados caso utilize a paginação
✅ Utilização de estruturas de códigos reconhecidas pelo mercado (a escolha do candidato)
✅ Injeção de dependencias
✅ Testes automatizados :)
✅ Qualquer item desenvolvido além do que foi pedido
## Realizei Atividades Extras nāo Solicitadas:
✅ O Símbolo/Nome da MadeInWeb que ficam no topo do aplicativo funcionam como botões para voltar a pagina anterior, facilitando a ergonomia ao precisar voltar paginas sem necessidade de esticar o dedo até a quina superior esquerda. 
✅ Gerei uma versão do TestFlight para vocês poderem realizar o teste do App sem precisar de um computador.
✅ Realizei o Build do projeto(bem como as posições dos componentes) para que o App seja disponível(e adaptado) também para iPad[iPadOS]
✅ Utilizei componentes da bibliotecas nativas do iOS/iPadOS, e isso permite que o aplicativo assuma o Modo escuro caso ele esteja ativado no sistema. Além de responder a gestos nativos do próprio sistema(que os usuários do aparelho já estão acostumados) como: Voltar para página anterior deslizado o dedo no telefone da esquerda para direita; Ou subir para o topo listagem de resultados da pesquisa só tocando no relógio(que fica na parte superior da tela).
    
_________________________________________________________________________________________________
    
# Instalação pelo computador:
## 1) Realize o “Clone” do projeto da branch “Master”  para seu Mac
## 2) Abra o projeto como Workspace ao abrir o arquivo "MadeInWeb BrenoMedeiros.xcworkspace”, pois isso fará o Xcode abrir o Projeto corretamente
## 3) Escolha um Simulador ou um iPhone/iPad/iPod conectado ao computador na parte superior da tela e clique "Command+R” para instalar e executar o Aplicativo
    
# Instalação sem Computador:
## 1) Baixe o App do TestFlight da apple para seu iPhone/iPad/iPod em: https://apps.apple.com/br/app/testflight/id899247664
## 2) Agora clique no link [ https://testflight.apple.com/v1/invite/f3f87717dfc347e8b7eebe76dbecc0d3f5887a034e8a43c5a16cc8fa38ff127359151aa6?ct=7XLL76M7KQ&advp=10000&platform=ios ] para baixar o aplicativo do teste-mobile através do TestFlight.

    

Abraço!
